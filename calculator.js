var displayString = '';
var score = 0;

var insertedNumber = '';

var numbersArray = [];
var count = 0;

var actualOperator;

var isEqual = false;
var isFloat = false;
var isSqrt = false;
var isMinus = false;

var canInsertOperator = false;

function addToDisplay(thisChar){
	
	var lastChar = displayString[displayString.length - 1];
	
	if(!isSqrt && (isNaN(thisChar) && isNaN(lastChar))){	//change operator. Example from + to -
		var newStr = displayString.slice(0, -1); // slice(0, -1) - slice all string without last character
		displayString = newStr + thisChar;
	}
	else displayString += thisChar;
	
	displayActualString();
}

function insert(keyNumber) {
	
	if(!isEqual){
		
		canInsertOperator = true;
		
		if(keyNumber == '.') isFloat = true;
		
		addToDisplay(keyNumber);
		insertedNumber += keyNumber;
	}
}

function addToArray(addThis){
	numbersArray[count] = addThis;
	count++;
}

function changeOperator(newOperator){
	if(newOperator != '=') actualOperator = newOperator;
	addToDisplay(newOperator);
}

function operatorSelect(operator){
	
	if(operator != '=' && insertedNumber == '' && displayString != '' && !canInsertOperator && !isSqrt) changeOperator(operator);
	
	if(canInsertOperator){
	
		//if '=' was pressed then next char is operator (not an number) so don't add operator to numbers array
		if(!isEqual){
			var holdNumber;
			
			if(isFloat){
				holdNumber = parseFloat(insertedNumber);
				isFloat = false;
			}		
			else holdNumber = parseInt(insertedNumber);
			
			addToArray(holdNumber);
		}
	
		if(isEqual){	//if '=' was pressed then next operator is select so we continue calculating then turn off score display and display upper string
			document.getElementById("scoreDisplay").innerHTML = '';
			displayActualString();
			isEqual = false;
		}
		
		if(isSqrt){
			numbersArray[numbersArray.length - 1] = Math.sqrt(numbersArray[numbersArray.length - 1]);
			isSqrt = false;
		}
	
		if(operator != '=' && displayString != '' && !isSqrt) addToDisplay(operator);
	
		if(numbersArray.length == 1) score = numbersArray[0];
		else calculate();
		
		if(operator != '=') actualOperator = operator;
		else {
			equal();
			isEqual = true;
		}
	
		insertedNumber = '';
		isMinus = false;
		
		if(operator != '=') canInsertOperator = false;
	}
}

function changeChar(){
	
	if(insertedNumber == '' && !isSqrt){	//first pressed set minus char
		isMinus = true;
		insertedNumber += '-';
		displayString += '-';
		displayActualString();
	}
	else if(insertedNumber[0] == '-' && insertedNumber.length < 2) {	//if second pressed delete minus char
		isMinus = false;
		insertedNumber = '';
		displayString = displayString.slice(0, -1);
		displayActualString();
	}
}

function equal(){
	displayString = score.toString();
	document.getElementById("display").innerHTML = '';
	actualOperator = '';
	isFloat = false;
	isSqrt = false;
	numbersArray = [];
	numbersArray[0] = score;
	displayScore();
	count = 0;
	score = 0;
	count++;
}

function squareRoot(){
	
	if(insertedNumber.length < 1 && !isSqrt){	//check if actual number is already a square root
		isSqrt = true;
		addToDisplay("\u221a");
	}
	else if(isSqrt && insertedNumber == ''){
		isSqrt = false;
		displayString = displayString.slice(0, -1);
		displayActualString();
	}
}

function comma(){
	
	if(insertedNumber != '' && !isFloat && canInsertOperator) insert('.');	//check if number was inserted and if this number is already a float
}

function calculate(){
	
	switch(actualOperator){
		case '+':
			score += numbersArray[numbersArray.length - 1];
		break;

		case '-':
			score -= numbersArray[numbersArray.length - 1];
		break;

		case '*':
			score *= numbersArray[numbersArray.length - 1];
		break;
	
		case '/':
			if(numbersArray[1] == 0 || numbersArray[numbersArray.length - 1] == 0){
				clearDisplay();
				divideByZero();
			}
			else score /= numbersArray[numbersArray.length - 1];
		break;
		
		default:
			
		break;
	}
	
	displayScore();
}

function clearDisplay(){
	displayString = '';
	insertedNumber = '';
	numbersArray = [];
	score = 0;
	count = 0;
	actualOperator = '';
	isEqual = false;
	isFloat = false;
	isSqrt = false;
	var isMinus = false;
	var canInsertOperator = false;
	document.getElementById("display").innerHTML = 0;
	document.getElementById("scoreDisplay").innerHTML = '';
}

function displayActualString(){
	document.getElementById("display").innerHTML = displayString;
}

function displayScore(){
	if(!Number.isInteger(score) && score.toString().length > 10){
		var newScore = parseFloat(score).toFixed(10);
		document.getElementById("scoreDisplay").innerHTML = newScore;
	}
	else document.getElementById("scoreDisplay").innerHTML = score;
}

function divideByZero(){
	var element = document.getElementById("containerAnimate");
	var opacity = 1;
	var myInterval = setInterval(frame, 25);
	
	function frame(){
		opacity -= 0.01;
		element.style.opacity = opacity;
	}
	
	document.getElementById("errorText").innerHTML = "Nice ! You broke it down. <br> REMEMBER <br> Never divide by zero !";
	document.getElementById("errorText").innerHTML += "<br> <button id='restartButton' onclick='reloadPage()'> Restart? </button> <br> or <br>";
	document.getElementById("errorText").innerHTML += "<button id='restartButton' onclick='closePage()'> Exit? </button>";
}

function reloadPage(){
	location.reload();
}

function closePage(){
	close();
}