# Javascript Calculator
Simple calculator made with CSS, HTML and Javascript. The calculator was made while learning javascript

## Calculator Functionalities
* You can add, subtract, multiply, divide and calculate square root
* You can use numbers with a comma
* You can change the value of the number to negative
* You can see score up to date
* You can change the inserted operator